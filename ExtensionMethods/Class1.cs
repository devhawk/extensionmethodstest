﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System.Runtime.CompilerServices
{
    internal class ExtensionAttribute : Attribute { }
}

namespace ExtensionMethods
{
    public static class StingExtensions
    {
        public static string SayHello(this string name)
        {
            return string.Format("Hello {0}", name);
        }
    }
}
